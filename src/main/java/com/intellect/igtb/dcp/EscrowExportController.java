package com.intellect.igtb.dcp;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.ICBXBackendResponse;
import com.intellect.igtb.dcp.export.controller.ExportServiceController;
import com.intellect.igtb.dcp.service.BuildingAccountSummary;
import com.intellect.igtb.dcp.service.BuildingAccountsGroupByMaster;
import com.intellect.igtb.dcp.service.MasterAccountsSummary;
import com.intellect.igtb.dcp.service.PrincipalExport;
import com.intellect.igtb.dcp.service.Transactions;

@RestController
@RequestMapping("/export")
public class EscrowExportController {

	@Autowired
	PrincipalExport principalExport;
	
	@Autowired
	Transactions transactions;
	
	@Autowired
	MasterAccountsSummary masterAccountsSummary; 
	
	@Autowired
	BuildingAccountSummary buildingAccountSummary; 
	
	@Autowired
	BuildingAccountsGroupByMaster buildingAccountsGroupByMaster;
	 

	@RequestMapping(value = "/principal", method = RequestMethod.POST)
	public ResponseEntity<ICBXBackendResponse> principal(@RequestBody final Map<String, Object> payload,
			@RequestHeader final Map<String, String> headers) {
		System.out.println("in principal");
		return new ExportServiceController(principalExport).execute(payload, headers);
	}
	
	
//	@RequestMapping(value = "/transcations", method = RequestMethod.POST)
//	public ResponseEntity<ICBXBackendResponse> transactions(@RequestBody final Map<String, Object> payload,
//			@RequestHeader final Map<String, String> headers) {
//		System.out.println("in transactions");
//		return new ExportServiceController(transactions).execute(payload, headers);
//	}
	
	@RequestMapping(value = "/master-accounts", method = RequestMethod.POST)
	public ResponseEntity<ICBXBackendResponse> masterAccounts(@RequestBody final Map<String, Object> payload,
			@RequestHeader final Map<String, String> headers) {
		System.out.println("in master");
		return new ExportServiceController(masterAccountsSummary).execute(payload, headers);
	}
	
	@RequestMapping(value = "/building-accounts", method = RequestMethod.POST)
	public ResponseEntity<ICBXBackendResponse> buildingAccounts(@RequestBody final Map<String, Object> payload,
			@RequestHeader final Map<String, String> headers) {
		System.out.println("in building-accounts");
		return new ExportServiceController(buildingAccountSummary).execute(payload, headers);
	}
	
	@RequestMapping(value = "/building-accounts-groupby", method = RequestMethod.POST)
	public ResponseEntity<ICBXBackendResponse> buildingAccountsGroupByMaster(@RequestBody final Map<String, Object> payload,
			@RequestHeader final Map<String, String> headers) {
		System.out.println("in building-accounts");
		return new ExportServiceController(buildingAccountsGroupByMaster).execute(payload, headers);
	}
	
	
	
	
	

}