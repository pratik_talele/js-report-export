package com.intellect.igtb.dcp.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.intellect.igtb.dcp.cloudbocommons.exception.CBXBusinessException;
import com.intellect.igtb.dcp.cloudbocommons.exception.CBXException;
import com.intellect.igtb.dcp.cloudbocommons.gqlactions.models.FieldValidationResult;
import com.intellect.igtb.dcp.export.model.ExportContext;
import com.intellect.igtb.dcp.export.model.Title;
import com.intellect.igtb.dcp.export.service.CBXSExportService;
import com.intellect.igtb.dcp.export.service.ReportService;
import com.intellect.igtb.dcp.utilities.Utils;

@Component
public class Transactions extends CBXSExportService {
	
	private static final Logger logger = LoggerFactory.getLogger(Transactions.class);

	private Utils utils=new Utils();

	HashMap<String, FieldValidationResult> valResponseMap = new HashMap<String, FieldValidationResult>();

	@Autowired
	ReportService reportService;


	@Override
	public ExportContext init(final Map<String, String> httpHeaders, final Map<String, Object> payload)
			throws CBXException {
		logger.debug("IN init - Transactions {}", payload.toString());
		exportContext = super.init(httpHeaders, payload);

		logger.debug("OUT init - Transactions");
		return exportContext;
	}
	
	@Override
	public Map<String, FieldValidationResult> validate() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getQueryName(Map<String, Object> payload) {
		// TODO Auto-generated method stub
		return "Transactions";
	}

	@Override
	public String getFormat(Map<String, Object> payload) {
		// TODO Auto-generated method stub
		return (String) payload.get("format");
	}

	@Override
	public Boolean isGrouped(Map<String, Object> payload) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Title getTitle(Map<String, Object> payload) {
		Title title = new Title();
		title.setKey((String) payload.get("summary_name"));
		return title;
	}

	@Override
	public String getHeaderBlocksConfig(Map<String, Object> payload) {
		String sHeaderBlocksConfig = reportService.loadContent("/grid_list/header.json");
		return sHeaderBlocksConfig;
//		return null;
	}

	@Override
	public String getGridConfigAsString(Map<String, Object> payload) {
		String sGridConfigData = reportService.loadContent("/grid_list/principalGrid.json");
		return sGridConfigData;
	}

	@Override
	public String getHeaderData(Map<String, Object> payload) {
//		String sHeaderData = "";
//		try {
//			Map<String, Object> vaAccountDetail = utils.getVaTransactionSummary(payload, exportContext);
//			sHeaderData = vaAccountDetail.get("va_account_detail").toString();
//
//		} catch (CBXBusinessException | CBXException e) {
//			logger.debug("Exception {}", e.getStackTrace().toString());
//		}
//		return sHeaderData;
		return "transaction data";
	}

	@Override
	public String getExportData(Map<String, Object> payload) {
		String sExportData = "";
		try {
			Map<String, Object> vaTransactionSummaryMap = utils.getVaTransactionSummary(payload, exportContext);
			sExportData = vaTransactionSummaryMap.get("va_transaction_summary").toString();

		} catch (CBXBusinessException | CBXException e) {
			logger.debug("Exception {}", e.getStackTrace().toString());
		}
		return sExportData;
	}

}
